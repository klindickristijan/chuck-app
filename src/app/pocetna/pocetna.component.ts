import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-pocetna',
  templateUrl: './pocetna.component.html',
  styleUrls: ['./pocetna.component.css']
})
export class PocetnaComponent {
  private randomJokeApi = 'https://api.chucknorris.io/jokes/random';
  private data: any = {};
  private allJokes = [];

  constructor(private http: Http) {
    this.getJokes();
    this.getData();
   }

  getData() {
    return this.http.get(this.randomJokeApi)
      .map((res: Response) => res.json())
  }

  getJokes() {
    this.getData().subscribe(joke => {
      console.log(joke);
      console.log(joke.value);
      this.allJokes.unshift(joke);
      console.log(this.allJokes);
    })
  }
   
  deleteJoke(joke) {
    for (let i = 0; i < this.allJokes.length; i++) {
      if (this.allJokes[i] == joke) {
        this.allJokes.splice(i,1);
      } 
    }
  }
}
