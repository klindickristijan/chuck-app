import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { RouterModule, Router, Routes } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Chuck Norris Jokes';
}
